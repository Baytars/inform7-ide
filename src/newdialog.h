/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2006-2010 Philip Chimento <philip.chimento@gmail.com>
 */

#ifndef NEWDIALOG_H
#define NEWDIALOG_H

#include "config.h"

#include <gtk/gtk.h>

GtkWidget *create_new_dialog(void);

#endif
